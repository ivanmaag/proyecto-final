package root.model.entities;

import lombok.Getter;
import lombok.Setter;
import org.aspectj.weaver.patterns.TypePatternQuestions;
import root.model.enums.FoodTypeEnum;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "food_type")
public class FoodType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_id")
    private Long id;

    @Column(name = "type_name")
    @Enumerated(EnumType.STRING)
    private FoodTypeEnum name;

    @OneToMany(mappedBy = "type", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Food> foods;


    public String getName() {
        return name.name();
    }

    public void setName(FoodTypeEnum name) {
        this.name = name;
    }
}
