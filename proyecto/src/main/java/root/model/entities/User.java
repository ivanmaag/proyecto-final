package root.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "user")
public class User implements UserDetails {

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "birth")
    private Date birth;

    @Column(name = "username")
    private String username;

    @JsonIgnore
    @Column(name = "password")
    @Length(min = 8)
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @JsonIgnore
    @Column(name = "active", columnDefinition = "BIT(1) default 0")
    private Boolean active = false;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_has_role",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "user_id",
                    foreignKey = @ForeignKey(name = "userrole_user_fk"))},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "role_id",
                    foreignKey = @ForeignKey(name = "userrole_role_fk"))}
    )
    private Set<UserRole> roles;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_has_permission",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "user_id",
                    foreignKey = @ForeignKey(name = "userpermission_user_fk"))},
            inverseJoinColumns = {@JoinColumn(name = "permission_id", referencedColumnName = "permission_id",
                    foreignKey = @ForeignKey(name = "userpermission_permission_fk"))}
    )
    private Set<Authority> permissions;

    @OneToMany(mappedBy = "nutritionist", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Advice> nutritionist;

    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Advice> patient;

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return permissions;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return this.active;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return this.active;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return this.active;
    }

    @Override
    public boolean isEnabled() {
        return this.active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return firstName;
    }

    public void setName(String name) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }

    @JsonIgnore
    public Set<Authority> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Authority> permissions) {
        this.permissions = permissions;
    }

    public Set<Advice> getAdvice() {
        return nutritionist;
    }

    public void setNutritionist(Set<Advice> nutritionist) {
        this.nutritionist = nutritionist;
    }

    public Set<Advice> getPatient() {
        return patient;
    }

    public void setPatient(Set<Advice> patient) {
        this.patient = patient;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birth=" + birth +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", active=" + active +
                ", roles=" + roles +
                ", permissions=" + permissions +
                ", nutritionist=" + nutritionist +
                ", patient=" + patient +
                '}';
    }
}
