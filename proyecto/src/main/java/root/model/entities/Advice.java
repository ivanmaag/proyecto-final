package root.model.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@Entity
@Table(name = "advice")
public class Advice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "advice_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "nutritionist_id", referencedColumnName = "user_id", foreignKey = @ForeignKey(name = "advice_nutritionist_fk"))
    private User nutritionist;

    @ManyToOne
    @JoinColumn(name = "patient_id", referencedColumnName = "user_id", foreignKey = @ForeignKey(name = "advice_patient_fk"))
    private User patient;
}
