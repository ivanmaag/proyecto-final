package root.model.enums;

public enum FoodTypeEnum {
    BREAKFAST,
    LUNCH,
    SNACK,
    DINNER,
    SNACKBAR
}
