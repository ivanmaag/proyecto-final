package root.exception;

public class ElementNotFoundException extends Exception {

    public ElementNotFoundException(String msg) {
        super(msg);
    }
}
