package root.service.interfaces;

import root.model.entities.User;
import root.model.entities.UserRole;
//import root.model.json.UserModel;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service("userDetailsService")
public interface UserService extends UserDetailsService {

    UserRole getRoleById(Long roleId);

    List<User> getAll();

    List<UserRole> getAllRoles();

    User findUserByEmail(String email);

    User getUser(Long id);

    User getCurrentUser();

    Set<User> findUserByActive(Boolean active);

    Set<User> findByRole(Long roleId);

    Set<User> findByRoleName(String roleName);

    //UserModel getCurrentUserModel(String langCode);
}
